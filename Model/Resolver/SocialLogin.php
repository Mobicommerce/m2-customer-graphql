<?php
declare(strict_types=1);

namespace Mobicommerce\CustomerGraphQl\Model\Resolver;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\AuthenticationInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Exception\GraphQlAuthenticationException;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\GraphQl\Model\Query\ContextInterface;
use Magento\Integration\Model\Oauth\TokenFactory as TokenModelFactory;
use Magento\CustomerGraphQl\Model\Customer\ExtractCustomerData;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\App\ObjectManager;

class SocialLogin implements ResolverInterface
{
     /**
     * @var AuthenticationInterface
     */
    private $authentication;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var AccountManagementInterface
     */
    private $accountManagement;

    /**
     * @param AuthenticationInterface $authentication
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $accountManagement
     */
    public function __construct(
        AuthenticationInterface $authentication,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $accountManagement,
        TokenModelFactory $tokenModelFactory,
        ExtractCustomerData $extractCustomerData,
        EncryptorInterface $encryptor,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    ) {
        $this->authentication = $authentication;
        $this->customerRepository = $customerRepository;
        $this->accountManagement = $accountManagement;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->extractCustomerData = $extractCustomerData;
        $this->encryptor = $encryptor;
        $this->customerFactory = $customerFactory;
        $this->_objectManager = ObjectManager::getInstance();        
    }
    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $store = $context->getExtensionAttributes()->getStore();
        $this->storeId = $store->getId();
        $this->websiteId = $store->getWebsite()->getWebsiteId();
       
        if (empty($args['input']['email'])) {
            throw new GraphQlInputException(__('Email is required Field'));
        }
        if (empty($args['input']['firstname'])) {
            throw new GraphQlInputException(__('firstname is required Field'));
        }
        if (empty($args['input']['lastname'])) {
            throw new GraphQlInputException(__('lastname is required Field'));
        }
        $data = $args['input'];
        $data['type'] = $args['type'];
         $customer = $this->doSocialLogin($data);
               
        $auth_token = $this->tokenModelFactory->create()->createCustomerToken($customer->getId())->getToken();
        return [            
            'token'=>$auth_token
        ];
    }

   public function doSocialLogin($data)
    {
        switch ($data['type']) {
            case 'facebook':
                $result = $this->doFacebookLogin($data);
                break;
            case 'google':
                $result = $this->doGoogleLogin($data);
                break;
            case 'twitter':
                $result = $this->doTwitterLogin($data);
                break;           
        }
        return $result;
    }

    public function doFacebookLogin($data)
    {
        $user_data = [
            'firstname' => $data['firstname'],
            'lastname'  => $data['lastname'],
            'email'     => $data['email']
            ];
        return $this->loginProcess($user_data);
    }

    public function doGoogleLogin($data)
    {
        $user_data = [
            'firstname' => $data['firstname'],
            'lastname'  => $data['lastname'],
            'email'     => $data['email']
            ];
        return $this->loginProcess($user_data);
    }

    function loginProcess($data)
    {
        $result = [];
        $store_id = $this->storeId;
        $website_id = $this->websiteId;       
        $customer = $this->getNewObject('\Magento\Customer\Model\Customer')
                ->setWebsiteId($website_id)
                ->loadByEmail($data['email']);
        if (!$customer->getData()) {
                $customer = $this->createCustomerMultiWebsite($data, $website_id, $store_id);
        }
        return $customer;       
    }

    public function createCustomerMultiWebsite($data, $website_id, $store_id)
    {        
        $customer = $this->customerFactory->create();
        $customer->setFirstname($data['firstname'])
            ->setLastname($data['lastname'])
            ->setEmail($data['email'])
            ->setWebsiteId($website_id)
            ->setStoreId($store_id)
            ->save();
        $password = $data['firstname'];
        $customer->setPassword($password);
        try {
            $customer->save();
        } catch (Exception $e) {
            
        }
        return $customer;
    }
    
   public function getNewObject($model, $data = []) {
        $model = $this->_objectManager->create($model, $data);
        return $model;
    }
}