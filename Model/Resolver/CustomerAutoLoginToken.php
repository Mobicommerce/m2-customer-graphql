<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_CustomerGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\CustomerGraphQl\Model\Resolver;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Encryption\EncryptorInterface;

class CustomerAutoLoginToken implements ResolverInterface
{
    protected $dataHelper;
 
    /**
     * @param DataProvider\Productsgraphql $productsgraphqlDataProvider
     */
    public function __construct(
        \Mobicommerce\CustomerGraphQl\Helper\Data $dataHelper,
        EncryptorInterface $encryptor
    ) {
        $this->dataHelper = $dataHelper;
        $this->encryptor = $encryptor;
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }

        /** @var Customer $customer */
        $customer = $value['model'];
        
        try {
            $return = $this->getEncryptToken(
                $this->dataHelper->encode($customer->getId())
            );
        } catch (LocalizedException $e) {
            $return = null;
        }

        return $return;
    }

    public function getEncryptToken($activation_code)
    {
        return $this->encryptor->encrypt($activation_code);
    }
}
