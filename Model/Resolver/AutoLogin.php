<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_CustomerGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\CustomerGraphQl\Model\Resolver;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\AuthenticationInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Exception\GraphQlAuthenticationException;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\GraphQl\Model\Query\ContextInterface;
use Magento\Integration\Model\Oauth\TokenFactory as TokenModelFactory;
use Magento\CustomerGraphQl\Model\Customer\ExtractCustomerData;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\Encryption\EncryptorInterface;

class AutoLogin implements ResolverInterface
{
     /**
      * @var AuthenticationInterface
      */
    private $authentication;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var AccountManagementInterface
     */
    private $accountManagement;

    /**
     * @param AuthenticationInterface $authentication
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $accountManagement
     */
    public function __construct(
        AuthenticationInterface $authentication,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $accountManagement,
        TokenModelFactory $tokenModelFactory,
        ExtractCustomerData $extractCustomerData,
        EncryptorInterface $encryptor,
        \Mobicommerce\CustomerGraphQl\Helper\Data $dataHelper
    ) {
        $this->authentication = $authentication;
        $this->customerRepository = $customerRepository;
        $this->accountManagement = $accountManagement;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->extractCustomerData = $extractCustomerData;
        $this->encryptor = $encryptor;
        $this->_dataHelper = $dataHelper;
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (empty($args['input']['auto_login_token'])) {
            throw new GraphQlInputException(__('Required parameter "auto_login_token" is missing'));
        }
        $auto_login_token = $args['input']['auto_login_token'];
        // phpcs:ignore Magento2.Functions.DiscouragedFunction
        $currentUserId = base64_decode($this->getDecryptToken($auto_login_token));
        $customer = $this->customerRepository->getById($currentUserId);
        $auth_token = $this->tokenModelFactory->create()->createCustomerToken($customer->getId())->getToken();
        return [
            'token'=>$auth_token
        ];
    }

    public function getDecryptToken($token)
    {
        return $this->encryptor->decrypt($token);
    }
}
